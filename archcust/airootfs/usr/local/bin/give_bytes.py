# Takes any human readable format and converts to bytes
# Credit for this script goes to Denziloe on Stackoverflow

#units = {"B": 1, "K": 10**3, "M": 10**6, "G": 10**9, "T": 10**12}
units = {"B": 1, "K": 2**10, "M": 2**20, "G": 2**30, "T": 2**40}

def byte_size(size):
    number, unit = size[:-1], size[-1:] 
    return int(float(number)*units[unit])


if __name__ == "__main__":
    example_strings = ["10.43K", "11G", "343.1M"]

    for example_string in example_strings:
        print(byte_size(example_string))
