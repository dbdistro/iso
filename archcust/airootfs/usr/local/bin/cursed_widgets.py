#/usr/bin/env python3

# Use to easily create widgets with curses.
# USE:
# from cursed_widgets import Widgets

import curses


#from subprocess import run

# run("", shell=True)

class Widgets:
    def __init__(self, screen, startX=0, startY=0, width=0, height=0, title=None):
        self.screen = screen
        self.startX = startX
        self.startY = startY
        self.width = width
        self.height = height
        self.title = title
        self._active = None
        """COLORS"""
        # Windows
        curses.init_pair(1, curses.COLOR_BLACK, curses.COLOR_WHITE)
        # Desktop
        curses.init_pair(2, curses.COLOR_BLACK, curses.COLOR_RED)
        # Select
        curses.init_pair(3, curses.COLOR_RED, curses.COLOR_BLACK)
        # Text Entry
        curses.init_pair(4, curses.COLOR_RED, curses.COLOR_WHITE)
        # Title
        curses.init_pair(5, curses.COLOR_WHITE, curses.COLOR_RED)
        # Active Widget
        curses.init_pair(6, curses.COLOR_RED, curses.COLOR_WHITE)

        self.widgets = {}


    def update(self):
        # Refresh the screen and capture events.
        self.screen.erase()
        self.draw()
        self.screen.refresh()
        key = self.screen.getch()
        self._update(key)
        self.screen.nodelay(False)
        
        return key
    
    
    def focus(self, widget_name):
        # Assigns widget to be in control by user.
        widget = self.widgets[widget_name]
        self._active = widget_name

        while True:
            key = self.update()
            # Selection or keybinding pressed, return result.
            # MsgBox with no keybindings will not remain active.
            if widget['type'] == 'msgBox' and widget['keybinds'] == None:
                break

            # If there are keybindings return them if they are pressed.
            if widget['keybinds'] != None and key != -1:
                if chr(key) in widget["keybinds"]:
                    widget["results"] = chr(key)
            # Return result from widget. EX: yesnoBox widget, user selects no, widget["results"] = "No"
            if widget["results"]:
                break

            
        results = widget["results"]
        widget["results"] = None
        self.screen.nodelay(True)
        return results

                               
################################
# Create Widget Functions
################################
    def msgBox(self, name, y, x, width, height, msg, keybinds=None):

        msg = str(msg)
        
        widget = {'x': x, 'y': y, 'width': width, 'height': height, 'msg': msg, 'type': 'msgBox', 'results': None, 'keybinds':keybinds, 'show':False}

        # Adds newline characters to msg, to get it to fit in msgBox.
        if '\n' in msg:
            self.widgets[name] = widget
        else:
            counter = 0
            new_msg = ""
            for letter in msg:
                if counter == (width - 1):
                    new_msg += ('\n' + letter)
                    counter = 0
                else:
                    new_msg += letter
                    counter += 1

            widget['msg'] = new_msg
            self.widgets[name] = widget
            

    def yesnoBox(self, name, y, x, width, height, msg, yes='Yes', no='No', keybinds=None):
        msg = str(msg)

        widget = {'y': y, 'x': x, 'width': width, 'height': height, 'msg': msg, 'yes': yes, 'no': no, 'select': no, 'type': 'yesnoBox', 'results': None, 'keybinds':keybinds, 'show':False}

        # Adds newline characters to msg, to get it to fit in msgBox.
        if '\n' in msg:
            pass
        else:
            counter = 0
            new_msg = ""
            for letter in msg:
                if counter == (width - 1):
                    new_msg += ('\n' + letter)
                    counter = 0
                else:
                    new_msg += letter
                    counter += 1

            widget['msg'] = new_msg

        self.widgets[name] = widget
        

    def formBox(self, name, y, x, questions, width=None, height=None, button="Ok", keybinds=None):
        widget = {'name': name, 'y': y, 'x': x, 'width': width, 'height': height, 'questions': {}, 'select': questions[0], "type": "formBox", "largest question": 0, "button": button, 'results': None, 'keybinds':keybinds, 'show':False}

        # Create empty entry for questions and figure out which question is the longest.
        maxSize = 0
        for question in questions:
            widget['questions'][question] = ""

            if len(question) > maxSize:
                maxSize = len(question)
        widget['largest question'] = maxSize

        if width == None:
            widget['width'] = maxSize + 30
        if height == None:
            widget['height'] = len(questions) + 2
        
        self.widgets[name] = widget

        
    def selectBox(self, name, y, x, raw_choices, title=None, width=None, height=None, button="Ok", maxItems=7, keybinds=None, multiple_choices=False):
        widget = {'name': name, 'y': y, 'x': x, 'choices': [], 'width': width, 'height': height, 'select': 0, 'page': 0, "maxItems": maxItems, 'results': None, 'checked': None, 'button': button, "rightArrow": "-->", "leftArrow": "<--", 'type':'selectBox', 'keybinds': keybinds, 'show': False, 'multiple_choices': multiple_choices}


        if widget["multiple_choices"]:
            widget["checked"] = []

            # Gives user option to have some checked. EX: [*] apples or [ ] apples. [1st value choice, 2nd value True|False]
            new_raw_choices = []
            index = 0
            for choice in raw_choices:
                if type(choice) == list:
                    if choice[1]:
                        # Check by default
                        widget["checked"].append(choice[0])
                    new_raw_choices.append(choice[0])
                else:
                    new_raw_choices.append(choice)
                index += 1
            raw_choices = new_raw_choices
                    

        # is this a list?
        ## if so: append all checked items to checked
        maxSize = 0

        if title != None:
            widget['title'] = f"{title}:"
            maxSize = len(widget['title'])
        else:
            widget['title'] = None

        if width == None:
            # Get longest string.
            for choice in raw_choices:
                if len(choice) > maxSize:
                    maxSize = len(choice)

            widget['width'] = maxSize + 10

        if height == None:
            # Create right/left arrow if len is greater than 7.
            if len(raw_choices) > maxItems:
                widget['height'] = maxItems + 4
            else:
                widget['height'] = len(raw_choices) + 4

            if widget["title"] != None:
                widget['height'] + 4

        # Create choices, divide into groups of 7.
        choices = []
        group = []
        num = 0
        for choice in raw_choices:
            if num >= maxItems:
                group.append(choice)
                choices.append(group)
                group = []
                num = 0
            else:
                group.append(choice)
                num += 1
        # Will get choices that were not equal to 7 that.
        if len(group) != 0:
            choices.append(group)

        widget["choices"] = choices
                
        # Make choice equal to 1st option.
        widget["select"] = widget["choices"][0][0]

        self.widgets[name] = widget
        

    def loadingBox(self, name, y, x):
        widget = {'name': name, 'y': y, 'x': x, 'width': 60, 'height': 4, 'type': 'loadingBox', 'load': 0, 'results': None, 'keybinds': None, 'show':False}
        self.widgets[name] = widget


################################
# Helper Fuctions
################################
    def _update(self, key):
        for widget in self.widgets.keys():
            if widget == self._active:
                widget = self.widgets[widget]
                if widget['type'] == 'yesnoBox':
                    if key == curses.KEY_RIGHT:
                        if widget['select'] == widget['yes']:
                            widget['select'] = widget['no']
                        else:
                            widget['select'] = widget['yes']
                    elif key == curses.KEY_LEFT:
                        if widget['select'] == widget['yes']:
                            widget['select'] = widget['no']
                        else:
                            widget['select'] = widget['yes']
                    elif key == curses.KEY_ENTER or key == 10 or key == 13:
                        widget['results'] = widget['select']

                elif widget['type'] == "formBox":
                    questions = list(widget['questions'].keys())
                    if widget['select'] != widget['button']:
                        if key == curses.KEY_UP:
                            widget['select'] = questions[questions.index(widget['select'])-1]
                        elif key == curses.KEY_DOWN:
                            if questions.index(widget['select']) == len(questions) - 1:
                                widget['select'] = questions[0]
                            else:
                                widget['select'] = questions[questions.index(widget['select'])+1]
                    if key == 9:
                        # select button tab key
                        if widget['select'] != widget['button']:
                            widget['select'] = widget['button']
                        else:
                            widget['select'] = questions[0]
                    if key == 10 or key == 13:
                        if widget['select'] == widget['button']:
                            widget['results'] = widget['questions']
                        else:
                    
                            # text entry mode
                            x = widget['x'] + widget['largest question'] + 2 + len(widget['questions'][widget['select']])
                            while True:
                                self.screen.erase()
                                self.draw()
                                self.screen.refresh()
                                self.screen.addch(widget['y'] + questions.index(widget['select']), x, '█', curses.color_pair(4) | curses.A_BLINK)
                                key = self.screen.getkey()
                                if key == "KEY_BACKSPACE" and len(widget['questions'][widget['select']]) != 0:
                                    widget['questions'][widget['select']] = widget['questions'][widget['select']][:-1]
                                    x -= 1
                                # Exit text mode
                                elif key == "\n":
                                    break
                                
                                # input text goes to widgets dictionary
                                elif len(widget['questions'][widget['select']]) <  widget['width'] - widget["largest question"] - 3 and key != "\t" and "KEY" not in key:
                                    widget['questions'][widget['select']] += key
                                    x += 1                       
            
                elif widget['type'] == "selectBox":
                    # Down Key
                    if key == curses.KEY_DOWN and widget['select'] != widget['button'] and widget["select"] != widget["rightArrow"] and widget["select"] != widget["leftArrow"]:
                        if widget["choices"][widget["page"]].index(widget["select"]) + 1 == len(widget["choices"][widget["page"]]):
                            widget["select"] = widget["choices"][widget["page"]][0]
                        else:
                            widget["select"] = widget["choices"][widget["page"]][widget["choices"][widget["page"]].index(widget["select"]) + 1]
                    # Up Key
                    if key == curses.KEY_UP and widget['select'] != widget['button'] and widget["select"] != widget["rightArrow"] and widget["select"] != widget["leftArrow"]:
                        widget['select'] = widget["choices"][widget["page"]][widget["choices"][widget["page"]].index(widget["select"]) - 1]
                    # Enter key
                    if key == 10 or key == 13 or key == curses.KEY_ENTER:
                        # PRESS BUTTON
                        if widget['select'] == widget['button']:
                            if widget['multiple_choices'] and widget['checked'] == []:
                                widget["results"] = 1
                            else:
                                widget['results'] = widget['checked']
                        
                        # PRESS LEFT ARROW
                        elif widget["select"] == widget["leftArrow"]:
                            if widget["page"] == 0:
                                widget["page"] = len(widget["choices"]) -1
                            else: 
                                widget["page"]-= 1
                        # PRESS RIGHT ARROW
                        elif widget["select"] == widget["rightArrow"]:
                            if widget["page"] == len(widget["choices"]) -1:
                                widget["page"] = 0
                            else:
                                widget["page"]+= 1
                        else:
                            # Check mark the option
                            if widget["multiple_choices"] == False:
                                widget['checked'] = widget['select']
                            else:
                                if widget['select'] in widget['checked']:
                                    # Remove - Uncheck
                                    widget['checked'].remove(widget['select'])
                                else:
                                    # Add to list - Check
                                    widget['checked'].append(widget['select'])
        
                    # Tab key
                    if key == 9:
                        # select button button
                        if widget['select'] != widget['button'] and widget['select'] != widget['rightArrow'] and widget['select'] != widget['leftArrow']:
                            widget['select'] = widget['button']
                        else:
                            widget['select'] = widget['choices'][widget["page"]][0]
                            
                    # Left ok right
                    ## Get the amount of choices.
                    length_of_choices = 0
                    for choices in widget["choices"]:
                        length_of_choices += len(choices)
                    if length_of_choices > widget["maxItems"]:
                        if key == curses.KEY_LEFT:
                            if widget["select"] == widget["button"]:
                                widget["select"] = widget["leftArrow"]
                            elif widget["select"] == widget["leftArrow"]:
                                widget["select"] = widget["rightArrow"]
                            elif widget["select"] == widget["rightArrow"]:
                                widget["select"] = widget["button"]
                        elif key == curses.KEY_RIGHT:
                            if widget["select"] == widget["button"]:
                                widget["select"] = widget["rightArrow"]
                            elif widget["select"] == widget["rightArrow"]:
                                widget["select"] = widget["leftArrow"]
                            elif widget["select"] == widget["leftArrow"]:
                                widget["select"] = widget["button"]
                    
                    
                    
                elif widget['type'] == 'loadingBox':
                    pass

    
    def draw(self):
        # Draw background
        self.screen.bkgd(' ', curses.color_pair(2))
        # Draw title
        if self.title != None:
            self.screen.addstr(0, 1, self.title, curses.color_pair(5) | curses.A_BOLD)
            self.screen.addstr(1, 1, "─"*(int(self.screen.getmaxyx()[1]) - 1), curses.color_pair(5) | curses.A_BOLD)

        for widget in self.widgets.keys():
            # Create border for widget
            # Create a different border around an active widget.
            if self.widgets[widget]["show"]:
                if widget == self._active:
                    widget = self.widgets[widget]
                
                    # Corner pieces
                    self.screen.addch(widget['y']-1, widget['x']-1, "┌", curses.color_pair(6))
                    self.screen.addch(widget['y'] + widget['height'], widget['x'] + widget['width'], "┘", curses.color_pair(6))
                    self.screen.addch(widget['y'] - 1, widget['x'] + widget['width'], "┐", curses.color_pair(6))
                    self.screen.addch(widget['y'] + widget['height'], widget['x'] - 1, "└", curses.color_pair(6))
        
                    # Top and bottom
                    for x in range(widget['x'], (widget['x'] + widget['width'])): 
                        self.screen.addch(widget['y'] - 1, x, "─", curses.color_pair(6))
                    for x in range(widget['x'], (widget['x'] + widget['width'])): 
                        self.screen.addch(widget['y'] + widget['height'], x, "─", curses.color_pair(6))
                    # Sides
                    for y in range(widget['y'], (widget['y'] + widget['height'])): 
                        self.screen.addch(y, widget['x'] - 1, "│", curses.color_pair(6))
                    for y in range(widget['y'], (widget['y'] + widget['height'])): 
                        self.screen.addch(y, widget['x'] + widget["width"], "│", curses.color_pair(6))
                else:
                    widget = self.widgets[widget]
                    # Corner pieces
                    self.screen.addch(widget['y']-1, widget['x']-1, "┌", curses.color_pair(1))
                    self.screen.addch(widget['y'] + widget['height'], widget['x'] + widget['width'], "┘", curses.color_pair(1))
                    self.screen.addch(widget['y'] - 1, widget['x'] + widget['width'], "┐", curses.color_pair(1))
                    self.screen.addch(widget['y'] + widget['height'], widget['x'] - 1, "└", curses.color_pair(1))

                    # Top and bottom
                    for x in range(widget['x'], (widget['x'] + widget['width'])): 
                        self.screen.addch(widget['y'] - 1, x, "─", curses.color_pair(1))
                    for x in range(widget['x'], (widget['x'] + widget['width'])): 
                        self.screen.addch(widget['y'] + widget['height'], x, "─", curses.color_pair(1))
                    # Sides
                    for y in range(widget['y'], (widget['y'] + widget['height'])): 
                        self.screen.addch(y, widget['x'] - 1, "│", curses.color_pair(1))
                    for y in range(widget['y'], (widget['y'] + widget['height'])): 
                        self.screen.addch(y, widget['x'] + widget["width"], "│", curses.color_pair(1))

                # Draw Shadows
                # Bottom
                for x in range(widget['x'], (widget['x'] + widget['width'] + 2)): 
                    self.screen.addch(widget['y'] + widget['height'] + 1, x, "█", curses.color_pair(1))          
                # Right
                for y in range(widget['y'], (widget['y'] + widget['height']) + 2): 
                                self.screen.addstr(y, widget['x'] + widget["width"] + 1, "██", curses.color_pair(1))
                               
                # Draw the box for the widget
                for y in range(widget['y'],  (widget['y'] + widget['height'])):
                    for x in range(widget['x'], (widget['x'] + widget['width'])): 
                        self.screen.addch(y, x, " ", curses.color_pair(1))
                                            
                # Adds buttons, messsages etc.
                if widget['type'] == "msgBox":
                    numx = 0
                    numy = 0
                    for letter in widget["msg"]:
                        if letter == "\n":
                            numy += 1
                            numx = 0
                        else:
                            self.screen.addch(widget['y'] + numy, widget['x'] + numx, letter, curses.color_pair(1))
                            numx += 1
                    
                elif widget['type'] == "yesnoBox":
                    # bar seperating buttons from msg box.                    
                    numx = 0
                    numy = 0
                    for letter in widget["msg"]:
                        if letter == "\n":
                            numy += 1
                            numx = 0
                        else:
                            self.screen.addch(widget['y'] + numy, widget['x'] + numx, letter, curses.color_pair(1))
                            numx += 1

                    self.screen.addstr(widget['y'] + widget['height'] - 2, widget['x'], '─'*widget['width'], curses.color_pair(1))
                    self.screen.addch(widget['y'] + widget['height'] - 2, widget['x'] - 1, '├', curses.color_pair(1))
                    self.screen.addch(widget['y'] + widget['height'] - 2, widget['x'] + widget['width'], '┤', curses.color_pair(1))
                    if widget['select'] == widget['yes']:
                        self.screen.addstr(widget['y'] + widget['height'] - 1, widget['x'] + (widget['width']) // 2 - len(widget['yes']) -6, f"< {widget['yes']} >", curses.color_pair(3) | curses.A_BOLD)
                        self.screen.addstr(widget['y'] + widget['height'] - 1, widget['x'] + (widget['width']) // 2 - len(widget['yes']) +6, f"< {widget['no']} >", curses.color_pair(1) | curses.A_BOLD)
                    else:
                        self.screen.addstr(widget['y'] + widget['height'] - 1, widget['x'] + (widget['width']) // 2 - len(widget['yes']) -6, f"< {widget['yes']} >", curses.color_pair(1) | curses.A_BOLD)
                        self.screen.addstr(widget['y'] + widget['height'] - 1, widget['x'] + (widget['width']) // 2 - len(widget['yes']) +6, f"< {widget['no']} >", curses.color_pair(3) | curses.A_BOLD)

                elif widget['type'] == "formBox":
                    
                    if widget['select'] == widget['button']:
                        self.screen.addstr(widget['y'] + widget['height'] - 1, widget['x'] + widget['width'] // 2 - len(widget['button']), f"< {widget['button']} >", curses.color_pair(3) | curses.A_BOLD)
                    else:
                        self.screen.addstr(widget['y'] + widget['height'] - 1, widget['x'] + widget['width'] // 2 - len(widget['button']), f"< {widget['button']} >", curses.color_pair(1))
                    num = 0
                    for key, value in widget['questions'].items():
                        question = key.rjust(widget['largest question'], " ")
                        question += ":"
                        if widget['select'] == key:
                            self.screen.addstr(widget['y'] + num, widget['x'], question, curses.color_pair(3) | curses.A_BOLD)
                            self.screen.addstr(widget['y'] + num, widget['x'] + len(question) + 1, value, curses.color_pair(1)) 
                        else:
                            self.screen.addstr(widget['y'] + num, widget['x'], question, curses.color_pair(1))
                            self.screen.addstr(widget['y'] + num, widget['x'] + len(question) + 1, value, curses.color_pair(1))
                        num += 1

                elif widget['type'] == "selectBox":
                    # Draw title
                    if widget['title'] != None:
                        self.screen.addstr(widget['y'], widget['x'], widget['title'], curses.color_pair(1))

                    # Draw button
                    if widget['select'] == widget["button"]:
                        self.screen.addstr(widget['y'] + widget['height'] - 1, (widget['x'] + widget['width'] // 2) - len(widget['button']), f"< {widget['button']} >", curses.color_pair(3) | curses.A_BOLD)
                    else:
                        self.screen.addstr(widget['y'] + widget['height'] - 1, (widget['x'] + widget['width'] // 2) - len(widget['button']), f"< {widget['button']} >", curses.color_pair(1))

                    # Right and left arrow
                    if len(widget["choices"]) > 1:
                        if widget['select'] == widget["rightArrow"]:
                            self.screen.addstr(widget['y'] + widget['height'] - 1, (widget['x'] + widget['width'] // 2) + 9 - len(widget['button']), widget["rightArrow"], curses.color_pair(3) | curses.A_BOLD)
                        else:
                            self.screen.addstr(widget['y'] + widget['height'] - 1, (widget['x'] + widget['width'] // 2) + 9 - len(widget['button']), widget["rightArrow"], curses.color_pair(1))
                        if widget["select"] == widget["leftArrow"]:
                            self.screen.addstr(widget['y'] + widget['height'] - 1, (widget['x'] + widget['width'] // 2) - 9 - len(widget['button']), widget["leftArrow"], curses.color_pair(3) | curses.A_BOLD)                    
                        else:
                            self.screen.addstr(widget['y'] + widget['height'] - 1, (widget['x'] + widget['width'] // 2) - 9 - len(widget['button']), widget["leftArrow"], curses.color_pair(1))                    

                    # Draw choices
                    if widget['title'] == None:
                        num = 0
                    else:
                        num = 2
                    for choice in widget['choices'][widget['page']]:
                        if widget["multiple_choices"]:
                            if choice == widget['select']:        
                                if choice in widget['checked']:
                                    self.screen.addstr(widget['y'] + num, widget['x'], f" [*] {choice}", curses.color_pair(3) | curses.A_BOLD)
                                else:
                                    self.screen.addstr(widget['y'] + num, widget['x'], f" [ ] {choice}", curses.color_pair(3) | curses.A_BOLD)
                            elif choice in widget['checked']:
                                self.screen.addstr(widget['y'] + num, widget['x'], f" [*] {choice}", curses.color_pair(1))                    
                            else:
                                self.screen.addstr(widget['y'] + num, widget['x'], f" [ ] {choice}", curses.color_pair(1))
                        else:
                            if choice == widget['select']:
                                    if choice == widget['checked']:
                                        self.screen.addstr(widget['y'] + num, widget['x'], f" (*) {choice}", curses.color_pair(3) | curses.A_BOLD)
                                    else:
                                        self.screen.addstr(widget['y'] + num, widget['x'], f" ( ) {choice}", curses.color_pair(3) | curses.A_BOLD)
                            elif choice == widget['checked']:
                                self.screen.addstr(widget['y'] + num, widget['x'], f" (*) {choice}", curses.color_pair(1))                    
                            else:
                                self.screen.addstr(widget['y'] + num, widget['x'], f" ( ) {choice}", curses.color_pair(1))
                        num += 1

                elif widget['type'] == "loadingBox":
                    for y in range(widget['height']):
                        self.screen.addstr(widget['y'] + y, widget['x'], " "*widget['load'])

            
# Create the widget give info: x y width height
# Draw widget
# Check for events within widget


if __name__ == "__main__":
    # Test out module.
    def run(screen):
        curses.start_color()
        curses.curs_set(False)
        screen.keypad(True)
        key = None
        height, width = screen.getmaxyx()
        
        widgets = Widgets(screen, title="Greeting")
        
        widgets.msgBox("output", 10, 10, 10, 10, "This is a message box. Deal with it!", keybinds=("a"))
        #widgets.yesnoBox("question", 10, 50, 20, 20, "Would you like some cookies?")
        widgets.selectBox("question", 10, 50, set("abcdefghijk"), maxItems=5, multiple_choices=True)
        
        # Program Loop
        while True:
            # screen.clear()

            widgets.widgets["question"]["show"]= True
            boolean = widgets.focus("question")
# 
            widgets.widgets["output"]["msg"] = str(boolean)
            
            widgets.widgets["output"]["show"]= True
            widgets.focus("output")

            # Test Multiple Select
            


            
            # widgets.draw()
            # screen.refresh()
            # #widgets.delete("output")


    curses.wrapper(run)
