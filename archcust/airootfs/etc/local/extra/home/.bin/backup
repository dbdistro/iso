#!/usr/bin/bash

# CREATED BY: Marcus Edward Butler
# DATE: 2023/05/04


hostname=$(cat /proc/sys/kernel/hostname)

# help
if [[ "$1" == "-h" || "$1" == "--help" ]]; then
	echo "This program takes a directory or directories as arguments split using a space character. This program will store the backup in a directory with the computer name. A confirmation will be given before backup is created."
	echo 
	echo "This program will NOT continue if:"
	echo "	-No directory specified"
	echo "	-The directory does not exist"
	echo "	-The directory is not a mountpoint"
	echo "	-You don't have write premissions"
	exit 0
fi

# Exit 1 if no arguments.
if [ ${#@} == 0 ]; then
	echo "ERROR: No directories specified."
	exit 1
fi

for i in "$@";
do
	echo $i
	# Check if all directories exist 
	if [ ! -d $i ];
	then
		echo "ERROR: The path \"$i\" does not exist."
		exit 1
	fi
	# Check if all directories are mounted	
	x=$(mount | grep $i)
	if [ ${#x} == 0 ]; then
		echo "ERROR: The path \"$i\" you specified is not a mountpoint."
		exit 1
	fi
	# Check for write premissions
	if [ ! -w $i ]; then
		echo "ERROR: You don't have write premissions for \"$i\"."
		exit 1
	fi
done



# Ask for confirmation
for i in "$@";
do
	echo "Creating backup in:" $i/$hostname
done

read -p "Is path correct?[y/n]"
echo

if [ $REPLY == y ]
# Backup
then
	for i in "$@";
	do
		echo CREATING BACKUP: $i/$hostname
		rsync -rtxvulHspE  --progress --delete  --exclude={'.config/BraveSoftware', '.config/qutebrowswer/greasemonkey', '.cache'} ~/ $i/$hostname
	done
fi

exit 0
