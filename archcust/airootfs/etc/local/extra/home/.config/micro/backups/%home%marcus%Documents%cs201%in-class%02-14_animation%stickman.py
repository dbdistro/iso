#!/usr/bin/env python3

from time import sleep
from subprocess import run


gun = ";=-"
bullet = '*'


player =[ f"""
    0
   /|\\{gun}
    |
   / \\
""",
f"""
    0
   /|\\{gun}
    |
    |
"""
]

def fire():
    for i in range(30):
        run("clear", shell=True)
        print(" "*i, bullet)
        sleep(0.01)
    run("clear", shell=True)
    print(gun)


for i in range(30):
    if i % 2:
        lines = player[0].split("\n")
        for line in lines:
            print(" "*i + line)
    else:
        lines = player[1].split("\n")
        for line in lines:
            print(" "*i + line)        
    sleep(0.1)
    run("clear", shell=True)

lines = player[0].split("\n")
for line in lines:
    print(" "*i + line)
    fire()    
    
