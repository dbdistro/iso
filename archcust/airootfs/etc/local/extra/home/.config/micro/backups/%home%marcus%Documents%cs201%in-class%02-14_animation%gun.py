#!/usr/bin/env python3

from subprocess import run
from time import sleep


gun = ";=-"
bullet = '*'

for i in range(30):
    run("clear", shell=True)
    print(gun, " "*i, bullet)
    sleep(0.01)
run("clear", shell=True)
print(gun)

