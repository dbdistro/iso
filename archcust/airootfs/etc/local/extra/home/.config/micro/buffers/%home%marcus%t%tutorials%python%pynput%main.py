I˙SerializedBuffer˙ EventHandler˙ Cursor˙ ModTime˙   8˙EventHandler˙ 	UndoStack˙ 	RedoStack˙   '˙TEStack˙ Top˙ Size   *˙Element˙ Value˙ Next˙   B˙	TextEvent˙ C˙ 	EventType Deltas˙ Time˙   Z˙Cursor˙ Loc˙ LastVisualX CurSelection˙ OrigSelection˙ Num   ˙Loc˙ X Y   ˙[2]buffer.Loc˙ ˙  ˙[]buffer.Delta˙ ˙  0˙Delta˙ Text
 Start˙ End˙   ˙Time˙   ţá˙      ţfrom pynput import keyboard

def on_press(key):
    try:
        print('alphanumeric key {0} pressed'.format(
            key.char))
    except AttributeError:
        print('special key {0} pressed'.format(
            key))

def on_release(key):
    print('{0} released'.format(
        key))
    if key == keyboard.Key.esc:
        # Stop listener
        return False

# Collect events until released
with keyboard.Listener(
        on_press=on_press,
        on_release=on_release) as listener:
    listener.join()

# ...or, in a non-blocking fashion:
listener = keyboard.Listener(
    on_press=on_press,
    on_release=on_release)
listener.start()  6     ÜŻs*<'WţÔ      6    ÜŻtjaţÔ 