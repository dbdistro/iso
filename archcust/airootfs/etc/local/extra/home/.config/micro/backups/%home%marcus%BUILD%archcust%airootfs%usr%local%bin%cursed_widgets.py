# Use to easily create widgets with curses.
# USE:
# from cursed_widgets import Widgets

import curses


#from subprocess import run

# run("", shell=True)


class Widgets():
    def __init__(self, screen, startX=0, startY=0, width=0, height=0, title=None):
        self.screen = screen
        self.startX = startX
        self.startY = startY
        self.width = width
        self.height = height
        self.title = title

        self.active = None

        """COLORS"""
        # Windows
        curses.init_pair(1, curses.COLOR_BLACK, curses.COLOR_WHITE)
        # Desktop
        curses.init_pair(2, curses.COLOR_BLACK, curses.COLOR_RED)
        # Select
        curses.init_pair(3, curses.COLOR_RED, curses.COLOR_BLACK)
        # Text Entry
        curses.init_pair(4, curses.COLOR_RED, curses.COLOR_WHITE)
        # Title
        curses.init_pair(5, curses.COLOR_WHITE, curses.COLOR_RED)
        
        self.widgets = []

    def msgBox(self, name, y, x, width, height, msg):

        msg = str(msg)
        
        widget = {'name':name, 'x': x, 'y': y, 'width': width, 'height': height, 'msg': msg, 'type': 'msgBox'}

        # Adds newline characters to msg, to get it to fit in msgBox.
        if '\n' in msg:
            self.widgets.append(widget)
        else:
            counter = 0
            new_msg = ""
            for letter in msg:
                if counter == (width - 1):
                    new_msg += ('\n' + letter)
                    counter = 0
                else:
                    new_msg += letter
                    counter += 1

            widget['msg'] = new_msg
            self.widgets.append(widget)
            self.active = widget['name']
            

    def yesnoBox(self, name, y, x, width, height, msg, yes='Yes', no='No'):
        msg = str(msg)

        widget = {'name': name, 'y': y, 'x': x, 'width': width, 'height': height, 'msg': msg, 'yes': yes, 'no': no, 'select': yes, 'type': 'yesnoBox', 'results': None}

        # Adds newline characters to msg, to get it to fit in msgBox.
        if '\n' in msg:
            pass
        else:
            counter = 0
            new_msg = ""
            for letter in msg:
                if counter == (width - 1):
                    new_msg += ('\n' + letter)
                    counter = 0
                else:
                    new_msg += letter
                    counter += 1

            widget['msg'] = new_msg

        self.widgets.append(widget)
        self.active = widget['name']
        

    def formBox(self, name, y, x, questions, width=None, height=None, button="Ok"):
        widget = {'name': name, 'y': y, 'x': x, 'width': width, 'height': height, 'questions': {}, 'select': questions[0], "type": "formBox", "largest question": 0, "button": button, 'results': None}

        # Create empty entry for questions and figure out which question is the longest.
        maxSize = 0
        for question in questions:
            widget['questions'][question] = ""

            if len(question) > maxSize:
                maxSize = len(question)
        widget['largest question'] = maxSize

        if width == None:
            widget['width'] = maxSize + 30
        if height == None:
            widget['height'] = len(questions) + 2
        
        self.widgets.append(widget)
        self.active = widget['name']

        
    def selectBox(self, name, y, x, choices, title=None, width=None, height=None, button="Ok"):
        widget = {'name': name, 'y': y, 'x': x, 'choices': choices, 'width': width, 'height': height, 'select': choices[0], 'results': None, 'checked': None, 'button': button, 'type':'selectBox'}

        maxSize = 0

        if title != None:
            widget['title'] = f"{title}:"
            maxSize = len(widget['title'])
        else:
            widget['title'] = None

        if width == None:
            # Get longest string.
            for choice in choices:
                if len(choice) > maxSize:
                    maxSize = len(choice)

            widget['width'] = maxSize + 10

        if height == None:
            widget['height'] = len(choices) + 4
            if widget["title"] != None:
                widget['height'] + 4

        self.widgets.append(widget)
        self.active = widget['name']              
        

    def loadingBox(self, name, y, x):
        widget = {'name': name, 'y': y, 'x': x, 'width': 60, 'height': 4, 'type': 'loadingBox', 'load': 0, 'results': None}


        self.widgets.append(widget)
        self.active = widget['name']

    def load(self, name, load):
        for widget in self.widgets:
            if widget['type'] == "loadingBox" and widget['name'] == name:
                widget['load'] = load
            
    
    def delete(self, name):
        # Delete widget
        for widget in self.widgets[:]:
            if widget['name'] == name:
                self.widgets.remove(widget)
                
        if len(self.widgets) == 0:
            self.active = None
                
    
    def update(self, key):
        for widget in self.widgets:
            if widget['name'] == self.active:

                if widget['type'] == 'yesnoBox':
                    if key == curses.KEY_RIGHT:
                        if widget['select'] == widget['yes']:
                            widget['select'] = widget['no']
                        else:
                            widget['select'] = widget['yes']
                    elif key == curses.KEY_LEFT:
                        if widget['select'] == widget['yes']:
                            widget['select'] = widget['no']
                        else:
                            widget['select'] = widget['yes']
                    elif key == curses.KEY_ENTER or key == 10 or key == 13:
                        widget['results'] = widget['select']

                elif widget['type'] == "formBox":
                    questions = list(widget['questions'].keys())
                    if widget['select'] != widget['button']:
                        if key == curses.KEY_UP:
                            widget['select'] = questions[questions.index(widget['select'])-1]
                        elif key == curses.KEY_DOWN:
                            if questions.index(widget['select']) == len(questions) - 1:
                                widget['select'] = questions[0]
                            else:
                                widget['select'] = questions[questions.index(widget['select'])+1]
                    if key == 9:
                        # select button button tab key
                        if widget['select'] != widget['button']:
                            widget['select'] = widget['button']
                        else:
                            widget['select'] = questions[0]
                    if key == 10 or key == 13:
                        if widget['select'] == widget['button']:
                            widget['results'] = widget['questions']
                        else:
                    
                            # text entry mode
                            x = widget['x'] + widget['largest question'] + 2 + len(widget['questions'][widget['select']])
                            while True:
                                self.screen.erase()
                                self.draw()
                                self.screen.refresh()
                                self.screen.addch(widget['y'] + questions.index(widget['select']), x, '█', curses.color_pair(4) | curses.A_BLINK)
                                key = self.screen.getkey()
                                if key == "KEY_BACKSPACE" and len(widget['questions'][widget['select']]) != 0:
                                    widget['questions'][widget['select']] = widget['questions'][widget['select']][:-1]
                                    x -= 1
                                # Exit text mode
                                elif key == "\n":
                                    break
                                
                                # input text goes to widgets dictionary
                                elif len(widget['questions'][widget['select']]) <  widget['width'] - widget["largest question"] - 3 and key != "\t" and "KEY" not in key:
                                    widget['questions'][widget['select']] += key
                                    x += 1                       
            
                elif widget['type'] == "selectBox":
                    # Down Key
                    if key == curses.KEY_DOWN and widget['select'] != widget['button']:
                        if widget["choices"].index(widget["select"]) + 1 == len(widget["choices"]):
                            widget["select"] = widget["choices"][0]
                        else:
                            widget["select"] = widget["choices"][widget["choices"].index(widget["select"]) + 1]
                    # Up Key
                    elif key == curses.KEY_UP and widget['select'] != widget['button']:
                        widget['select'] = widget["choices"][widget["choices"].index(widget["select"]) - 1]
                    # Enter key
                    elif key == 10 or key == 13 or key == curses.KEY_ENTER:
                        if widget['select'] == widget['button']:
                            widget['results'] = widget['checked']
                        else:
                            widget['checked'] = widget['select']
                    # Tab key
                    elif key == 9:
                        # select button button
                        if widget['select'] != widget['button']:
                            widget['select'] = widget['button']
                        else:
                            widget['select'] = widget['choices'][0]

                elif widget['type'] == 'loadingBox':
                    pass

    
    def draw(self):
        # Draw background
        self.screen.bkgd(' ', curses.color_pair(2))
        # Draw title
        if self.title != None:
            self.screen.addstr(0, 1, self.title, curses.color_pair(5) | curses.A_BOLD)
            self.screen.addstr(1, 1, "─"*(int(self.screen.getmaxyx()[1]) - 1), curses.color_pair(5) | curses.A_BOLD)

        for widget in self.widgets:
            # Create border for widget
            # Corner pieces
            self.screen.addch(widget['y']-1, widget['x']-1, "┌", curses.color_pair(1))
            self.screen.addch(widget['y'] + widget['height'], widget['x'] + widget['width'], "┘", curses.color_pair(1))
            self.screen.addch(widget['y'] - 1, widget['x'] + widget['width'], "┐", curses.color_pair(1))
            self.screen.addch(widget['y'] + widget['height'], widget['x'] - 1, "└", curses.color_pair(1))

            # Top and bottom
            for x in range(widget['x'], (widget['x'] + widget['width'])): 
                self.screen.addch(widget['y'] - 1, x, "─", curses.color_pair(1))
            for x in range(widget['x'], (widget['x'] + widget['width'])): 
                self.screen.addch(widget['y'] + widget['height'], x, "─", curses.color_pair(1))
            # Sides
            for y in range(widget['y'], (widget['y'] + widget['height'])): 
                self.screen.addch(y, widget['x'] - 1, "│", curses.color_pair(1))
            for y in range(widget['y'], (widget['y'] + widget['height'])): 
                self.screen.addch(y, widget['x'] + widget["width"], "│", curses.color_pair(1))

            # Draw Shadows
            # Bottom
            for x in range(widget['x'], (widget['x'] + widget['width'] + 2)): 
                self.screen.addch(widget['y'] + widget['height'] + 1, x, "█", curses.color_pair(1))          
            # Right
            for y in range(widget['y'], (widget['y'] + widget['height']) + 2): 
                            self.screen.addstr(y, widget['x'] + widget["width"] + 1, "██", curses.color_pair(1))
                           
            # Draw the box for the widget
            for y in range(widget['y'],  (widget['y'] + widget['height'])):
                for x in range(widget['x'], (widget['x'] + widget['width'])): 
                    self.screen.addch(y, x, " ", curses.color_pair(1))
                                        
            # Adds buttons, messsages etc.
            if widget['type'] == "msgBox":
                numx = 0
                numy = 0
                for letter in widget["msg"]:
                    if letter == "\n":
                        numy += 1
                        numx = 0
                    else:
                        self.screen.addch(widget['y'] + numy, widget['x'] + numx, letter, curses.color_pair(1))
                        numx += 1
                
            elif widget['type'] == "yesnoBox":
                # bar seperating buttons from msg box.                    
                numx = 0
                numy = 0
                for letter in widget["msg"]:
                    if letter == "\n":
                        numy += 1
                        numx = 0
                    else:
                        self.screen.addch(widget['y'] + numy, widget['x'] + numx, letter, curses.color_pair(1))
                        numx += 1

                self.screen.addstr(widget['y'] + widget['height'] - 2, widget['x'], '─'*widget['width'], curses.color_pair(1))
                self.screen.addch(widget['y'] + widget['height'] - 2, widget['x'] - 1, '├', curses.color_pair(1))
                self.screen.addch(widget['y'] + widget['height'] - 2, widget['x'] + widget['width'], '┤', curses.color_pair(1))
                if widget['select'] == widget['yes']:
                    self.screen.addstr(widget['y'] + widget['height'] - 1, widget['x'] + (widget['width']) // 2 - len(widget['yes']) -6, f"< {widget['yes']} >", curses.color_pair(3) | curses.A_BOLD)
                    self.screen.addstr(widget['y'] + widget['height'] - 1, widget['x'] + (widget['width']) // 2 - len(widget['yes']) +6, f"< {widget['no']} >", curses.color_pair(1) | curses.A_BOLD)
                else:
                    self.screen.addstr(widget['y'] + widget['height'] - 1, widget['x'] + (widget['width']) // 2 - len(widget['yes']) -6, f"< {widget['yes']} >", curses.color_pair(1) | curses.A_BOLD)
                    self.screen.addstr(widget['y'] + widget['height'] - 1, widget['x'] + (widget['width']) // 2 - len(widget['yes']) +6, f"< {widget['no']} >", curses.color_pair(3) | curses.A_BOLD)

            elif widget['type'] == "formBox":
                
                if widget['select'] == widget['button']:
                    self.screen.addstr(widget['y'] + widget['height'] - 1, widget['x'] + widget['width'] // 2 - len(widget['button']), f"< {widget['button']} >", curses.color_pair(3) | curses.A_BOLD)
                else:
                    self.screen.addstr(widget['y'] + widget['height'] - 1, widget['x'] + widget['width'] // 2 - len(widget['button']), f"< {widget['button']} >", curses.color_pair(1))
                num = 0
                for key, value in widget['questions'].items():
                    question = key.rjust(widget['largest question'], " ")
                    question += ":"
                    if widget['select'] == key:
                        self.screen.addstr(widget['y'] + num, widget['x'], question, curses.color_pair(3) | curses.A_BOLD)
                        self.screen.addstr(widget['y'] + num, widget['x'] + len(question) + 1, value, curses.color_pair(1)) 
                    else:
                        self.screen.addstr(widget['y'] + num, widget['x'], question, curses.color_pair(1))
                        self.screen.addstr(widget['y'] + num, widget['x'] + len(question) + 1, value, curses.color_pair(1))
                    num += 1

            elif widget['type'] == "selectBox":
                # Draw title
                if widget['title'] != None:
                    self.screen.addstr(widget['y'], widget['x'], widget['title'], curses.color_pair(1))

                # Draw button
                if widget['select'] == widget["button"]:
                    self.screen.addstr(widget['y'] + widget['height'] - 1, (widget['x'] + widget['width'] // 2) - len(widget['button']), f"< {widget['button']} >", curses.color_pair(3) | curses.A_BOLD)
                else:
                    self.screen.addstr(widget['y'] + widget['height'] - 1, (widget['x'] + widget['width'] // 2) - len(widget['button']), f"< {widget['button']} >", curses.color_pair(1))

                # Draw choices
                if widget['title'] == None:
                    num = 0
                else:
                    num = 2
                for choice in widget['choices']:
                    if choice == widget['select']:
                        if choice == widget['checked']:
                            self.screen.addstr(widget['y'] + num, widget['x'], f" (*) {choice}", curses.color_pair(3) | curses.A_BOLD)
                        else:
                            self.screen.addstr(widget['y'] + num, widget['x'], f" ( ) {choice}", curses.color_pair(3) | curses.A_BOLD)

                    elif choice == widget['checked']:
                        self.screen.addstr(widget['y'] + num, widget['x'], f" (*) {choice}", curses.color_pair(1))                    
                    else:
                        self.screen.addstr(widget['y'] + num, widget['x'], f" ( ) {choice}", curses.color_pair(1))
                    num += 1

            elif widget['type'] == "loadingBox":
                for y in range(widget['height']):
                    self.screen.addstr(widget['y'] + y, widget['x'], " "*widget['load'])

            
# Create the widget give info: x y width height
# Draw widget
# Check for events within widget


if __name__ == "__main__":
    # Test out module.
    def run(screen):
        curses.start_color()
        curses.curs_set(False)
        screen.keypad(True)
        key = None
        height, width = screen.getmaxyx()
        
        widgets = Widgets(screen, title="Greeting")

        #widgets.yesnoBox("question", 5, 30, 20, 5, "Install MS-DOS 7.1?")
        #widgets.msgBox("greeting", 40, 30, 14, 5, "Hello World!")
        #widgets.formBox("form", 20, 20, ('Username', 'Password', 'Confirm Password'))
        #widgets.selectBox("drivePart", 2, 2, ("/dev/sdb 500GB", "/dev/snc 256GB"), title="Seect Drive")
        #widgets.active = 'question'
        #widgets.loadingBox("test", 20, 20)
    
        widgets.delete("text")
        widgets.msgBox("text", height //2+10, width//2-40, 80, 10, f"Partitioning drive...\n\n/dev/sda\n├── /dev/sda1\n├── /dev/sda2\n└── /dev/sda3")
        widgets.msgBox("text", height //2+10, width//2-40, 80, 10, f"Set timezone to America/Chicago...")
        widgets.msgBox("text", height //2+10, width//2-40, 80, 10, "Chroot into flashed drive...\n\nSoft link America/Chicago timezone.")
        widgets.msgBox("text", height //2+10, width//2-40, 80, 10, "Chroot into flashed drive...\n\nSet Locale.")
        widgets.msgBox("text", height //2+10, width//2-40, 80, 10, "Chroot into flashed drive...\n\nSetup Network Manger.")
        widgets.msgBox("text", height //2+10, width//2-40, 80, 10, "Chroot into flashed drive...\n\nInitialize the RAM file system.")
        widgets.msgBox("text", height //2+10, width//2-40, 80, 10, f"Chroot into flashed drive...\n\nPut marcus into groups.\nsys,network,power,adm,wheel,optical,rfkill,video,storage,audio,users,scanner,lp\nAny user who is apart of the wheel group has sudo privilages.")
        widgets.msgBox("text", height //2+10, width//2-40, 80, 10, f"Chroot into flashed drive...\n\nPut marcus into groups:\nsys,network,power,adm,wheel,optical,rfkill,video,storage,audio,users,scanner,lp\n\n\n\n\nAny user who is apart of the wheel group has sudo privilages.")
        widgets.msgBox("text", height //2+10, width//2-40, 80, 10, f"Chroot into flashed drive...\n\nChange marcus and roots shell to zsh.")
        widgets.msgBox("text", height //2+10, width//2-40, 80, 10, f"Chroot into flashed drive...\n\nSetup marcus and roots password.\nRoots password will be equal to marcus's password.")      
        widgets.msgBox("text", height //2+10, width//2-40, 80, 10, f"Chroot into flashed drive...\n\nSetup bootloader GRUB...\nWill detect other Operating Systems.") 
        widgets.msgBox("text", height //2+10, width//2-40, 80, 10, f"Chroot into flashed drive...\n\nMake marcus apart of the wheel group,\nWhich will give marcus sudo privilages.") 
        widgets.msgBox("text", height //2+10, width//2-40, 80, 10, "Copy configs over...\n\nLeftWM") 
        widgets.msgBox("text", height //2+10, width//2-40, 80, 10, "Setup faillock config... \nEntering the password wrong will NOT result in being locked out.")
        widgets.msgBox("text", height //2+10, width//2-40, 80, 10, "Give all files in /home/{form['Username']}\nread and write premssions for {form['Username']}...") 
        widgets.msgBox("text", height //2+10, width//2-40, 80, 10, "Done...{OSNAME} is installed on /dev/{drive}\nIt is recommend to reboot your computer.") 

        widgets.loadingBox("installer", height//2 - 4//2, width//2 - 60//2)
        
        # Program Loop
        while True:
            screen.clear()
            #widgets.active = "question"
            widgets.update(key)
            #widgets.msgBox("output", 20, 10, 10, 10, key)

            
            widgets.draw()
            screen.refresh()
            key = screen.getch()
            #widgets.delete("output")


    curses.wrapper(run)
