import pygame
from pygame.locals import *


class Camera:
    def __init__(self, width, height, x, y, startCameraUp, startCameraDown, zoom=0, noClip=False):
        self.surface = pygame.Surface((width, height))

        self.startCameraUp = startCameraUp
        self.startCameraDown = startCameraDown
        self.moving = True
        
        self.x = x
        self.y = y

    def move(self, direction, pixels):
        if direction == "right":
            self.x -= pixels
        elif direction == "left":
            self.x += pixels
        elif direction == "down":
            self.y -= pixels
        elif direction == "up":
            self.y += pixels

    def drawCamera(self, screen):
        screen.blit(self.surface, (self.x, self.y))

    def drawSprite(self, image, pos):
        self.surface.blit(image, pos)
        
    def drawBackground(self, bgColor):
        self.surface.fill(bgColor)


class Player:
    def __init__(self, x, y, image, costumes):
        self.rect = pygame.Rect(x, y, costumes[0][2], costumes[0][3])
        self.image = image
        self.costumes = costumes

        self.costume = 0
        self.speed = 7
        self.up = False
        self.down = False
        self.right = False
        self.left = False

    def events(self, event):
        if event.type == KEYDOWN:
            if event.key == K_UP:
                self.up = True
            if event.key == K_DOWN:
                self.down = True
            if event.key == K_RIGHT:
                self.right = True
            if event.key == K_LEFT:
                self.left = True

        if event.type == KEYUP:
            if event.key == K_UP:
                self.up = False
            if event.key == K_DOWN:
                self.down = False
            if event.key == K_RIGHT:
                self.right = False
            if event.key == K_LEFT:
                self.left = False

    def update(self, camera, screen, walls):
        # Update Movement
        if self.up:
            # Change costume
            self.costume = 3
            self.move("up")
            # Moves camera
            if self.rect.centery > camera.startCameraDown and self.rect.centery < camera.startCameraUp:
                camera.move("up", self.speed)
        if self.down:
            self.costume = 7
            self.move("down")
            # Moves camera
            if self.rect.centery < camera.startCameraUp and self.rect.centery > camera.startCameraDown:
                camera.move("down", self.speed)
        if self.right:
            self.costume = 5
            self.move("right")
        if self.left:
            self.costume = 1
            self.move("left")
        # Change costume
        if self.left and self.up:
            self.costume = 2
        if self.right and self.up:
            self.costume = 4
        if self.right and self.down:
            self.costume = 6
        if self.left and self.down:
            self.costume = 8
        # If not moving
        if self.left == False and self.right == False and self.up == False and self.down == False:
            self.costume = 0
        
        # Get rect touching
        for wall in walls:
            if self.rect.colliderect(wall):
                collide = wall
                collideRight = collide.right
                collideLeft = collide.left
                collideTop = collide.top
                collideBottom = collide.bottom
                break

        if collide:
            # Get closeset collision
            collestCollision = (collideRight.right - self.rect.centerx)**2 + (collide.centery - self.rect.centery)**2

        # # Moving Right
        # if self.down and collide:
            # testPlayer = self.rect
            # testPlayer.y -= self.speed
            # if testPlayer.colliderect(collide) == False:
                # self.rect = testPlayer
                # collide = False
        # if self.up and collide:
            # testPlayer = self.rect
            # testPlayer.y += self.speed
            # if testPlayer.colliderect(collide) == False:
                # self.rect = testPlayer
                # collide = False
        # if self.right and collide:
            # testPlayer = self.rect
            # testPlayer.x -= self.speed
            # if testPlayer.colliderect(collide) == False:
                # self.rect = testPlayer
                # collide = False
        # if self.left and collide:
            # testPlayer = self.rect
            # testPlayer.x += self.speed
            # if testPlayer.colliderect(collide) == False:
                # self.rect = testPlayer
                # collide = False
    
    def move(self, direction):
        if direction == "up":
            self.rect.y -= self.speed
        elif direction == "down":
            self.rect.y += self.speed
        elif direction == "right":
            self.rect.x += self.speed
        elif direction == "left":
            self.rect.x -= self.speed
        
    
    def draw(self, surface):
        #for costume in self.costumes:
        surface.blit(self.image, self.rect, area=(self.costumes[self.costume][0], self.costumes[self.costume][1], self.costumes[self.costume][2], self.costumes[self.costume][3]))
