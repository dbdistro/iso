;==========================================================
;
;
;   ██████╗  ██████╗ ██╗  ██╗   ██╗██████╗  █████╗ ██████╗
;   ██╔══██╗██╔═══██╗██║  ╚██╗ ██╔╝██╔══██╗██╔══██╗██╔══██╗
;   ██████╔╝██║   ██║██║   ╚████╔╝ ██████╔╝███████║██████╔╝
;   ██╔═══╝ ██║   ██║██║    ╚██╔╝  ██╔══██╗██╔══██║██╔══██╗
;   ██║     ╚██████╔╝███████╗██║   ██████╔╝██║  ██║██║  ██║
;   ╚═╝      ╚═════╝ ╚══════╝╚═╝   ╚═════╝ ╚═╝  ╚═╝╚═╝  ╚═╝
;
;
;   To learn more about how to configure Polybar
;   go to https://github.com/polybar/polybar
;
;   The README contains a lot of information
;
;==========================================================

[color]
;background = ${xrdb:color0:#222}
;background = #4e4949
;background = #555555
background = 282525
;background-alt = #444
;foreground = ${xrdb:color7:#222}
foreground = #f8f8f2
;foreground-alt = #555
primary = #ffb52a
secondary = #ffffff
alert = #bd2c40
modulefg = #ffffff
modulefg-alt = #f7f7f7

; Active Shade
shade1 = #282629
shade2 = #37474F
shade3 = #455A64
shade4 = #546E7A
shade5 = #607D8B
shade6 = #78909C
shade7 = #90A4AE
shade8 = #B0BEC5

trans = #00000000
white = #FFFFFF
black = #000000
tray-background = #60000000

[settings]
screenchange-reload = true
;compositing-background = xor
;compositing-background = screen
;compositing-foreground = source
;compositing-border = over
;pseudo-transparency = false

[global/wm]
margin-top = 0
margin-bottom = 0

[bar/barbase]
;width = ${env:width}
width = 100%
;height = 24
height = 20
offset-x = 0
offset-y = 0
monitor = ${env:monitor}
fixed-center = true
bottom = false

background = ${color.background}
foreground = ${color.foreground}

line-size = 3
line-color = #f00


padding = 2

module-margin-left = 1
module-margin-right = 1


font-0 = "Noto Sans:size=11:weight=semibold;3"
modules-center = date
modules-right = cpu_label cpu mem_label memory sto_label filesystem uptime_label uptime vol_label pulseaudio web_label network battery_label battery

[bar/mainbar0]
inherit = bar/barbase
modules-left = workspace0 host title
tray-position = right
tray-detached = false
tray-maxsize = 14
tray-offset-x = 0
tray-offset-y = 0
tray-padding = 5
tray-scale = 1.0
tray-background = ${color.tray-background}

[module/workspace0]
type = custom/script
exec = leftwm-state -w 0 -t $SCRIPTPATH/template.liquid
tail = true

scroll-up = leftwm-command FocusPreviousTag
scroll-down = leftwm-command FocusNextTag

[bar/mainbar1]
inherit = bar/barbase
;modules-left = workspace1
modules-left = workspace1 host title

[module/workspace1]
type = custom/script
exec = leftwm-state -w 1 -t $SCRIPTPATH/template.liquid
tail = true

scroll-up = leftwm-command FocusPreviousTag
scroll-down = leftwm-command FocusNextTag

[bar/mainbar2]
inherit = bar/barbase
modules-left = workspace2 host title


[module/workspace2]
type = custom/script
exec = leftwm-state -w 2 -t $SCRIPTPATH/template.liquid
tail = true

scroll-up = leftwm-command FocusPreviousTag
scroll-down = leftwm-command FocusNextTag

[bar/mainbar3]
inherit = bar/barbase
modules-left = workspace3 host title

[module/workspace3]
type = custom/script
exec = leftwm-state -w 3 -t $SCRIPTPATH/template.liquid
tail = true

scroll-up = leftwm-command FocusPreviousTag
scroll-down = leftwm-command FocusNextTag

#####################################################################

[module/cpu]
type = internal/cpu
interval = 2
;format-prefix = " "
;format-prefix-foreground = ${color.shade4}
;format-foreground = ${color.modulefg}
;format-background = ${color.shade6}
;format-underline = #f90000
label = %percentage:2%%

[module/memory]
type = internal/memory
interval = 2
;format-prefix = " "
;format-prefix-foreground = ${color.foreground-alt}
;format-underline = #4bffdc
;format-foreground = ${color.modulefg}
;format-background = ${color.shade5}
label = %mb_used% / %mb_total%

[module/battery]
type = internal/battery

; This is useful in case the battery never reports 100% charge
; Default: 100
full-at = 99

; format-low once this charge percentage is reached
; Default: 10
; New in version 3.6.0
low-at = 5

; Use the following command to list batteries and adapters:
; $ ls -1 /sys/class/power_supply/
battery = BAT0
adapter = ADP1

; If an inotify event haven't been reported in this many
; seconds, manually poll for new values.
;
; Needed as a fallback for systems that don't report events
; on sysfs/procfs.
;
; Disable polling by setting the interval to 0.
;
; Default: 5
poll-interval = 5

[module/wlan]
type = internal/network
interface = net1
interval = 3.0

format-connected = <ramp-signal> <label-connected>
;format-connected-foreground = ${color.modulefg}
;format-connected-background = ${color.shade5}
;format-connected-underline = #9f78e1
label-connected = %essid%

format-disconnected =
;format-disconnected = <label-disconnected>
;format-disconnected-underline = ${self.format-connected-underline}
;label-disconnected = %ifname% disconnected
;label-disconnected-foreground = ${color.foreground-alt}

ramp-signal-0 = 
ramp-signal-1 = 
ramp-signal-2 = 
ramp-signal-3 = 
ramp-signal-4 = 
ramp-signal-foreground = ${color.foreground-alt}

[module/eth]
type = internal/network
interface = enp0s31f6
interval = 3.0

;format-connected-underline = #55aa55
format-connected-prefix = " "
format-connected-prefix-foreground = ${color.foreground-alt}
label-connected = IP %local_ip%

format-disconnected =
;format-disconnected = <label-disconnected>
;format-disconnected-underline = ${self.format-connected-underline}
;label-disconnected = %ifname% disconnected
;label-disconnected-foreground = ${color.foreground-alt}
;format-connected-background = ${color.shade5}
;format-connected-foreground = ${color.modulefg}

[module/pulseaudio]
type = internal/pulseaudio

format-volume = <label-volume>
format-volume-background = ${color.background}
format-volume-foreground = ${color.foreground}
label-volume = %percentage%%
;label-volume-foreground = ${color.foreground}
label-volume-foreground = #00ff00

label-muted = %percentage%%
;label-muted-foreground = #888
label-muted-foreground = #ff0000

bar-volume-width = 10
bar-volume-foreground-0 = #55aa55
bar-volume-foreground-1 = #55aa55
bar-volume-foreground-2 = #55aa55
bar-volume-foreground-3 = #55aa55
bar-volume-foreground-4 = #55aa55
bar-volume-foreground-5 = #f5a70a
bar-volume-foreground-6 = #ff5555
bar-volume-gradient = false
bar-volume-indicator = |
bar-volume-indicator-font = 2
bar-volume-fill = ─
bar-volume-fill-font = 2
bar-volume-empty = ─
bar-volume-empty-font = 2
bar-volume-empty-foreground = ${color.foreground-alt}

[module/temperature]
type = internal/temperature
thermal-zone = 0
warn-temperature = 60

format = <label>
format-underline = #f50a4d
format-warn = <label-warn>
format-warn-underline = ${self.format-underline}
format-background = ${color.background}
format-foreground = ${color.foreground}

label = %temperature-c%
label-warn =    %temperature-c%
label-warn-foreground = ${color.secondary}

[module/date]
type = internal/date
interval = 5
time = %I:%M %p %Y/%m/%d 
time-alt =  %H:%M %p %Y/%m/%d
;format-prefix = 
;format-prefix-foreground = ${color.foreground-alt}
;format-background = ${color.shade2}
;format-foreground = ${color.modulefg}
;format-underline = #0a6cf5
label = %date% %time%

[module/kernel]
type = custom/script
exec = uname -r
tail = false
interval = 1024

format-foreground = ${color.foreground}
format-background = ${color.background}
format-prefix = "  "
format-prefix-foreground = ${color.foreground}
format-underline =${color.foreground}

click-left = xfce4-taskmanager
click-middle = xfce4-taskmanager
click-right = alacritty -e bpytop

[module/quote]
type = custom/text
content = Follow the white rabbit. Knock. Knock. 
content-foreground = ${color.foreground}
content-background = ${color.background}
content-prefix = "  "
content-prefix-foreground = ${color.foreground}
;content-underline = ${color.foreground}

[module/arch-updates]
type = custom/script
exec = $HOME/.config/leftwm/themes/current/scripts/check-arch-updates.sh
interval = 1000
label = Arch: %output%
format-foreground = ${color.foreground} 
format-background = ${color.background}
format-prefix = "Update"
format-prefix-foreground = ${color.foreground}
format-underline = ${color.foreground}
;click-left = urxvt
click-middle = xfce4-terminal
;click-right = alacritty

[module/aur-updates]
type = custom/script
exec = ~/.config/leftwm/themes/current/scripts/check-aur-updates.sh
interval = 1000
label = Aur: %output%
format-foreground = ${color.foreground}
format-background = ${color.background}
format-prefix = "  "
format-prefix-foreground = ${color.foreground}
format-underline = ${color.foreground}
click-left = urxvt
click-middle = xfce4-terminal
click-right = alacritty

[module/application-menu]
type = custom/text
content = "  "
content-foreground = #FFF
click-left = xfce4-appfinder
click-middle = xfce4-appfinder
click-right = alacritty

[module/break]
type = custom/text
content = |
content-padding = 0
;content-margin = 0
content-foreground = ${color.foreground}
content-background =  ${color.background}
;content-underline = ${color.adapta-cyan}

[module/filesystem]
type = internal/fs

; Mountpoints to display
mount-0 = /

label-mounted = %used% / %total%

; Seconds to sleep between updates
; Default: 30
interval = 10

; Display fixed precision values
; Default: false
fixed-values = true

; Spacing (number of spaces, pixels, points) between entries
; Default: 2
spacing = 4

; Default: 90
; New in version 3.6.0
warn-percentage = 75

[module/sysmenu]
type = custom/text
content = " 襤 "
content-foreground = ${color.foreground}
click-left = archlinux-logout
click-right = archlinux-logout

####################################################

[module/bspwm]
type = internal/bspwm

; Only show workspaces defined on the same output as the bar
; NOTE: The bspwm and XRandR monitor names must match, which they do by default.
; But if you rename your bspwm monitors with bspc -n this option will no longer
; behave correctly.
; Default: true
pin-workspaces = true

; Output mode flags after focused state label
; Default: false
inline-mode = false

; Create click handler used to focus workspace
; Default: true
enable-click = false

; Create scroll handlers used to cycle workspaces
; Default: true
enable-scroll = false

; Set the scroll cycle direction 
; Default: true
reverse-scroll = false

; Use fuzzy (partial) matching on labels when assigning 
; icons to workspaces
; Example: code;♚ will apply the icon to all workspaces 
; containing 'code' in the label
; Default: false
fuzzy-match = true

; Only scroll through occupied workspaces
; Default: false
; New in version 3.6.0
occupied-scroll = true

[module/i3]
type = internal/i3

; Only show workspaces defined on the same output as the bar
;
; Useful if you want to show monitor specific workspaces
; on different bars
;
; Default: false
pin-workspaces = true

; Show urgent workspaces regardless of whether the workspace is actually hidden 
; by pin-workspaces.
;
; Default: false
; New in version 3.6.0
show-urgent = true

; This will split the workspace name on ':'
; Default: false
strip-wsnumbers = true

; Sort the workspaces by index instead of the default
; sorting that groups the workspaces by output
; Default: false
index-sort = true

; Create click handler used to focus workspace
; Default: true
enable-click = false

; Create scroll handlers used to cycle workspaces
; Default: true
enable-scroll = false

; Wrap around when reaching the first/last workspace
; Default: true
wrapping-scroll = false

; Set the scroll cycle direction 
; Default: true
reverse-scroll = false

; Use fuzzy (partial) matching on labels when assigning 
; icons to workspaces
; Example: code;♚ will apply the icon to all workspaces 
; containing 'code' in the label
; Default: false
fuzzy-match = true

; ws-icon-[0-9]+ = <label>;<icon>
; NOTE: The <label> needs to match the name of the i3 workspace
; Neither <label> nor <icon> can contain a semicolon (;)
ws-icon-0 = 1;♚
ws-icon-1 = 2;♛
ws-icon-2 = 3;♜
ws-icon-3 = 4;♝
ws-icon-4 = 5;♞
ws-icon-default = ♟
; NOTE: You cannot skip icons, e.g. to get a ws-icon-6
; you must also define a ws-icon-5.
; NOTE: Icon will be available as the %icon% token inside label-*

; Available tags:
;   <label-state> (default) - gets replaced with <label-(focused|unfocused|visible|urgent)>
;   <label-mode> (default)
format = <label-state> <label-mode>

; Available tokens:
;   %mode%
; Default: %mode%
label-mode = %mode%
label-mode-padding = 2
label-mode-background = #e60053

; Available tokens:
;   %name%
;   %icon%
;   %index%
;   %output%
; Default: %icon% %name%
label-focused = %index%
label-focused-foreground = #ffffff
label-focused-background = #3f3f3f
label-focused-underline = #fba922
label-focused-padding = 4

; Available tokens:
;   %name%
;   %icon%
;   %index%
;   %output%
; Default: %icon% %name%
label-unfocused = %index%
label-unfocused-padding = 4

; Available tokens:
;   %name%
;   %icon%
;   %index%
;   %output%
; Default: %icon% %name%
label-visible = %index%
label-visible-underline = #555555
label-visible-padding = 4

; Available tokens:
;   %name%
;   %icon%
;   %index%
;   %output%
; Default: %icon% %name%
label-urgent = %index%
label-urgent-foreground = #000000
label-urgent-background = #bd2c40
label-urgent-padding = 4

; Separator in between workspaces
label-separator = |
label-separator-padding = 2
label-separator-foreground = #ffb52a



[module/workspaces]
type = internal/xworkspaces

; Only show workspaces defined on the same output as the bar
;
; Useful if you want to show monitor specific workspaces
; on different bars
;
; Default: false
pin-workspaces = true

; Create click handler used to focus desktop
; Default: true
enable-click = true

; Create scroll handlers used to cycle desktops
; Default: true
enable-scroll = true

; icon-[0-9]+ = <desktop-name>;<icon>
; NOTE: The desktop name needs to match the name configured by the WM
; You can get a list of the defined desktops using:
; $ xprop -root _NET_DESKTOP_NAMES
icon-0 = 1;
icon-1 = 2;
icon-2 = 3;
icon-3 = 4;
icon-4 = 5;
icon-default = 


; Available tags:
;   <label-monitor>
;   <label-state> - gets replaced with <label-(active|urgent|occupied|empty)>
; Default: <label-state>
format = <label-state>
format-background = ${color.background}
format-padding = 0

; Available tokens:
;   %name%
; Default: %name%
label-monitor = %name%

; Available tokens:
;   %name%
;   %icon%
;   %index%
; Default: %icon%  %name%
label-active = %icon%
label-active-foreground = ${color.red}
label-active-overline = ${color.red}

; Available tokens:
;   %name%
;   %icon%
;   %index%
; Default: %icon%  %name%
label-occupied = %icon%
label-occupied-foreground = ${color.yellow}

; Available tokens:
;   %name%
;   %icon%
;   %index%
; Default: %icon%  %name%
label-urgent = %icon%
label-urgent-foreground = ${color.green}

; Available tokens:
;   %name%
;   %icon%
;   %index%
; Default: %icon%  %name%
label-empty = %icon%

label-active-padding = 1
label-urgent-padding = 1
label-occupied-padding = 1
label-empty-padding = 1

[module/host]
type = custom/script
exec = $HOME/.config/leftwm/themes/current/scripts/host-username.sh

[module/network]
type = custom/script
exec = $HOME/.config/leftwm/themes/current/scripts/check-network.sh
tail = false
interval = 12

[module/get-vol]
type = custom/script
exec = $HOME/.config/leftwm/themes/current/scripts/pipevolume.sh
tail = false
interval = 1

[module/uptime_label]
type = custom/text
content = UP
content-foreground = #b4b4b4

[module/uptime]
type = custom/script
exec = $HOME/.config/leftwm/themes/current/scripts/uptime-polybar.sh
tail = false
interval = 60

[module/cpu_label]
type = custom/text
content = CPU
content-foreground = #b4b4b4

[module/mem_label]
type = custom/text
content = MEM
content-foreground = #b4b4b4

[module/sto_label]
type = custom/text
content = DD
content-foreground = #b4b4b4

[module/vol_label]
type = custom/text
content = VOL
content-foreground = #b4b4b4

[module/web_label]
type = custom/text
content = WEB
content-foreground = #b4b4b4

# [module/battery_label]
# type = custom/text
# content = BAT
# content-foreground = #b4b4b4

[module/battery_label]
type = custom/script
exec = $HOME/.config/leftwm/themes/current/scripts/battery_label


[module/title]
type = internal/xwindow
; Available tags:
;   <label> (default)
format = <label>
format-background = #f00
format-foreground = #000
format-padding = 4

; Available tokens:
;   %title%
; Default: %title%
label = %title%
label-maxlen = 40

; Used instead of label when there is no window title
; Available tokens:
;   None
label-empty = Empty
label-empty-foreground = #707880

