# Marcus Ed. Butler
# 02/16/2023
# REVISION 1: 2023/03/10
# Uses amixer to get master volume level. Works with Pipewire.

#!/bin/bash

UNMUTE='%{F#f00}'
MUTE='%{F#0f0}'
NC='%{-F' # No Color

vol=$(awk -F"[][]" '/Left:/ { print $2 }' <(amixer sget Master))
isMute=$(amixer get Master | tail -2 | grep -c '\[on\]')

if [ $isMute == 2 ]; then
	echo ${MUTE}$vol${NC}
else
	echo  ${UNMUTE}$vol${NC}
fi

