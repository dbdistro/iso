#!/usr/bin/bash

# CREATED BY: Marcus Edward Butler
# START: 1:34pm 01/11/2023
# END:   1:50 01/11/2023
# DESCRIPTION:
#    Get username and machine name and print in a nice format.


echo "%{F#e61937}$(whoami)@$(cat /proc/sys/kernel/hostname)"
