#!/usr/bin/bash

# Fix for Virtual Manager Network. (I dont want to run this everytime I reboot my computer.)
virsh net-list --all
virsh net-start default
