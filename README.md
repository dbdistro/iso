# dbDistro Installation iso
![](assets/discs/v2.0/dbD2.0-disc.png)

**NOTE:** This project is still in development.

[RELEASES/DOWNLOADS](https://gitlab.com/dbdistro/iso/-/releases)

## Description
Please note that dbD is still a work in progress, dbD1.1 contains a lot of bugs.

dbD short for distro butler is a Distro, is a Linux Distro created by Marcus Butler. db Distro is built from a custom base Arch Linux installer and a custom compiled iso using the Archiso kit. The installation script is programed mostly in Python, and the rest in bash.

## Compile from source<br><br>**WARNING: I don't have the two required repos uploaded yet!**
1) Install the following packages.<br>
`paru -Sy archiso`
<br>or<br>
`yay -Sy archiso`

2) Clone the source.<br>
`git clone http://projects.dbdistro/iso`

3) Clone repos required to create iso.
(**INCOMPLETE:** I don't have the repos uploaded yet.)<br>
`git clone http://projects.dbdistro/iso-creation-repo`<br>
`git clone http://projects.dbdistro/iso-run-repo`<br><br>
Use a text editor to change the pacman.conf file located in `iso/archcust/pacman.conf` to point to the `iso-creation-repo`<br>
```
[custom]
SigLevel = Never
Server = file:///home/<username>/<path_to_repo>/iso-creation-repo
```
Do the same for `iso-run-repo`. `iso-run-repo` pacman.conf located in `iso/archcust/airootfs/etc/pacman.conf`


3) Change working directory to the root directory.<br>
`cd <location_of_repo>/iso`

4) Make sure work directory is empty.<br>
`rm -rf work/*`

2) Create the iso. (This will take a while, time depends on your hardware. Took me 15 minutes to create.)<br>
`mkarchiso -v -w work -o out archcust`

3) When iso finishes compiling iso will be in the out directory.<br>
`cd out`

## For More Information See the Arch Wiki
[archISO](https://wiki.archlinux.org/title/archiso)
