#!/usr/bin/env python3

# Form on whats your favorite Linux Distro.
raw_choices = ["Linux Mint", "Ubuntu", "Arch", "Arco", "Arch Craft", "dbD", "Alpine", "SuicdeOS", "DTOS", "Fadora", "Manjaro", "Kail Linux", "Xubuntu"]

# 5 different menus
choices = []
menu = []
num = 0
for choice in raw_choices:
    if num >= 4:
        menu.append(choice)
        choices.append(menu)
        menu = []
        num = 0
    else:
        menu.append(choice)
        num += 1
if len(menu) != 0:
    choices.append(menu)


print(choices[1])
